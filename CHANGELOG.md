# [2.1.0](https://gitlab.com/to-be-continuous/sonar/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([8f761df](https://gitlab.com/to-be-continuous/sonar/commit/8f761df6d04fd93b7c679b6e86ea887646de49b5))

## [2.0.1](https://gitlab.com/to-be-continuous/sonar/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([4bfbfe5](https://gitlab.com/to-be-continuous/sonar/commit/4bfbfe596561f67ec522b96079074460d8d7362d))

## [2.0.0](https://gitlab.com/to-be-continuous/sonar/compare/1.3.2...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([b8f2b40](https://gitlab.com/to-be-continuous/sonar/commit/b8f2b40a430565e551a13a70a595df1b9203bede))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.3.2](https://gitlab.com/to-be-continuous/sonar/compare/1.3.1...1.3.2) (2021-06-15)

### Bug Fixes

* prevent shallow git clone (required by Sonar Scanner) ([7a3ba87](https://gitlab.com/to-be-continuous/sonar/commit/7a3ba8764a65ae3ea85928d804a6e93b59c53059))

## [1.3.1](https://gitlab.com/to-be-continuous/sonar/compare/1.3.0...1.3.1) (2021-06-15)

### Bug Fixes

* autodetect MR when a milestone is here ([e85c45e](https://gitlab.com/to-be-continuous/sonar/commit/e85c45e9a43aeb078a7c9dcc9523508e03ba29bd))

## [1.3.0](https://gitlab.com/to-be-continuous/sonar/compare/1.2.0...1.3.0) (2021-06-10)

### Features

* move group ([d8850f2](https://gitlab.com/to-be-continuous/sonar/commit/d8850f2e6cf93c8403632dbb83cad76876c25677))

## [1.2.0](https://gitlab.com/Orange-OpenSource/tbc/sonar/compare/1.1.0...1.2.0) (2021-06-08)

### Features

* **sonar:** autodetect Merge Request from current branch ([139c6d1](https://gitlab.com/Orange-OpenSource/tbc/sonar/commit/139c6d18a2a83976e49bd0cbce10f4b40876d6d8))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/sonar/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([e1071c6](https://gitlab.com/Orange-OpenSource/tbc/sonar/commit/e1071c61264a13e81f11a637b627ec5a3486d06d))

## 1.0.0 (2021-05-06)

### Features

* initial release ([b18ab7c](https://gitlab.com/Orange-OpenSource/tbc/sonar/commit/b18ab7c79f40b7ea91cd26807ab8eebd08d4b332))
